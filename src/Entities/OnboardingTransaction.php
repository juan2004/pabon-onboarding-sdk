<?php

namespace Pabon\OnboardingSdk\Entities;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use PlacetoPay\Base\Traits\LoaderTrait;

class OnboardingTransaction extends Transaction
{
    use LoaderTrait;

    protected ?array $person = null;
    protected ?string $locale = null;
    protected ?string $approval_url = null;
    protected ?string $denial_url = null;

    protected ?int $requestID = null;

    protected ?array $additional = [];

    public function __construct(array $data)
    {
        $this->status = Status::quick(Status::ST_PENDING, ReasonCodes::PENDING_TRANSACTION);
        $this->id = 'system-'.uniqid();

        $this->load($data, ['person', 'locale', 'approval_url', 'denial_url', 'additional', 'requestID']);
    }

    public function additional(?string $key = null, $default = null)
    {
        if ($key) {
            return $this->additional[$key] ?? $default;
        }

        return $this->additional;
    }

    public function setAdditional(?array $additional): self
    {
        $this->additional = $additional;

        return $this;
    }

    public function mergeAdditional(array $additionalExtra): self
    {
        $this->additional = array_replace($this->additional, $additionalExtra);

        return $this;
    }

    public function getPerson(): ?array
    {
        return $this->person;
    }

    public function getLocale(): ?string
    {
        return $this->locale;
    }

    public function getApprovalUrl(): ?string
    {
        return $this->approval_url;
    }

    public function getDenialUrl(): ?string
    {
        return $this->denial_url;
    }

    public function getRequestID(): ?int
    {
        return $this->requestID;
    }
}
