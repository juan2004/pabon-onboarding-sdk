<?php

namespace Pabon\OnboardingSdk;

use PlacetoPay\Base\Entities\Status;
use Pabon\OnboardingSdk\Constants\Operations;
use Pabon\OnboardingSdk\Entities\OnboardingTransaction;
use Pabon\OnboardingSdk\Exceptions\ParserException;
use Pabon\OnboardingSdk\Support\ParserManager;
use Pabon\OnboardingSdk\Entities\Settings;
use Pabon\OnboardingSdk\Support\SettingsResolver;
use PlacetoPay\Tangram\Carriers\RestCarrier;
use PlacetoPay\Tangram\Events\Dispatcher;
use PlacetoPay\Tangram\Exceptions\InvalidSettingException;
use PlacetoPay\Tangram\Listeners\HttpLoggerListener;
use PlacetoPay\Tangram\Services\BaseGateway;

class Gateway extends BaseGateway
{
    protected Settings $settings;
    protected RestCarrier $carrier;
    protected ParserManager $parserManager;

    /**
     * @throws InvalidSettingException
     */
    public function __construct(array $settings)
    {
        $this->settings = new Settings($settings, SettingsResolver::create($settings));
        $this->carrier = new RestCarrier($this->settings->client());
        $this->parserManager = new ParserManager($this->settings);

        $this->addEventDispatcher();
    }

    public function validateIdentity(OnboardingTransaction $transaction): Status
    {
        return $this->process(Operations::VALIDATE_IDENTITY, $transaction)->status();
    }

    public function query(OnboardingTransaction $transaction): Status
    {
        return $this->process(Operations::QUERY, $transaction)->status();
    }

    private function process(string $operation, OnboardingTransaction $transaction): OnboardingTransaction
    {
        try {
            $parser = $this->parserManager->getParser($operation);
            $carrierDataObjet = $this->parseRequest($operation, $transaction, $parser);
            $this->carrier->request($carrierDataObjet);
            $this->parseResponse($carrierDataObjet, $parser);

            return $transaction;
        } catch (ParserException $e) {
            throw ParserException::invalidOperation($operation);
        }
    }

    public function settings(): Settings
    {
        return $this->settings;
    }

    /**
     * @throws InvalidSettingException
     */
    protected function addEventDispatcher(): void
    {
        // Add Events Required
        if ($loggerSettings = $this->settings->loggerSettings()) {
            $this->setLoggerContext($loggerSettings);

            $listener = new HttpLoggerListener(
                $loggerSettings,
                $this->settings->providerName(),
                $this->settings->simulatorMode()
            );

            $this->carrier->setDispatcher(Dispatcher::create($listener->getEventsMethodsToDispatcher()));
        }
    }

    protected function setLoggerContext(&$loggerSettings): void
    {
        // Add masking rules for logs
        $loggerSettings['context'] = [
            'request' => [],
        ];
    }
}
