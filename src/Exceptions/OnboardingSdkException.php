<?php

namespace Pabon\OnboardingSdk\Exceptions;

use Exception;

class OnboardingSdkException extends Exception
{
    public static function forDataNotProvided(string $message = ''): self
    {
        return new self($message);
    }
}
