<?php

namespace Pabon\OnboardingSdk\Exceptions;

class ParserException extends OnboardingSdkException
{
    public static function invalidOperation(string $operation): self
    {
        return new self(sprintf('Operation %s is not supported', $operation));
    }
}
