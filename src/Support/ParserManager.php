<?php

namespace Pabon\OnboardingSdk\Support;

use Pabon\OnboardingSdk\Constants\Operations;
use Pabon\OnboardingSdk\Exceptions\ParserException;
use Pabon\OnboardingSdk\Parsers\QueryParser;
use Pabon\OnboardingSdk\Parsers\ValidateIdentityParser;
use PlacetoPay\Tangram\Entities\BaseSettings;

class ParserManager
{
    protected const OPERATIONS_PARSERS = [
        Operations::VALIDATE_IDENTITY => ValidateIdentityParser::class,
        Operations::QUERY => QueryParser::class,
    ];

    protected array $parsers = [];
    protected BaseSettings $settings;

    public function __construct(BaseSettings $settings)
    {
        $this->settings = $settings;
    }

    public function getParser(string $operation)
    {
        $this->validateOperation($operation);

        if (! isset($this->parsers[$operation])) {
            $this->createParser($operation);
        }

        return $this->parsers[$operation];
    }

    protected function createParser(string $operation): void
    {
        $parserName = self::OPERATIONS_PARSERS[$operation];
        $this->parsers[$operation] = new $parserName($this->settings);
    }

    /**
     * @throws ParserException
     */
    protected function validateOperation(string $operation): void
    {
        if (! isset(self::OPERATIONS_PARSERS[$operation])) {
            throw ParserException::invalidOperation($operation);
        }
    }
}
