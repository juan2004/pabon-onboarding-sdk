<?php

namespace Pabon\OnboardingSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Helpers\ArrayHelper;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\OnboardingSdk\Constants\Endpoints;
use Pabon\OnboardingSdk\Constants\Fields;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class ValidateIdentityParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'POST',
            'endpoint' => Endpoints::BASE_SANDBOX.Endpoints::VALIDATE_IDENTITY,
        ]));

        return [
            'json' => ArrayHelper::filter([
                Fields::PERSON => $carrierDataObject->transaction()->getPerson(),
                Fields::LOCALE => $carrierDataObject->transaction()->getLocale(),
                Fields::APPROVAL_URL => $carrierDataObject->transaction()->getApprovalUrl(),
                Fields::DENIAL_URL => $carrierDataObject->transaction()->getDenialUrl(),
            ]),
        ];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }
}
