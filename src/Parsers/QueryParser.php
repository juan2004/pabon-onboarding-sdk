<?php

namespace Pabon\OnboardingSdk\Parsers;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use PlacetoPay\Base\Messages\Transaction;
use Pabon\OnboardingSdk\Constants\Endpoints;
use Pabon\OnboardingSdk\Constants\ExceptionMessages;
use Pabon\OnboardingSdk\Exceptions\OnboardingSdkException;
use PlacetoPay\Tangram\Contracts\CarrierDataObjectContract;
use PlacetoPay\Tangram\Contracts\ParserHandlerContract;

class QueryParser implements ParserHandlerContract
{
    public function parserRequest(CarrierDataObjectContract $carrierDataObject): array
    {
        $carrierDataObject->setOptions(array_merge([
            'method' => 'GET',
            'endpoint' => $this->endpoint($carrierDataObject->transaction()->getRequestID()),
        ]));

        return [];
    }

    public function parserResponse(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickOk(
            ReasonCodes::APPROVED_TRANSACTION,
            $carrierDataObject->response()->getBody()->getContents(),
        ));

        return $carrierDataObject->transaction();
    }

    public function errorHandler(CarrierDataObjectContract $carrierDataObject): Transaction
    {
        $carrierDataObject->transaction()->setStatus(Status::quickFailed(
            ReasonCodes::INVALID_RESPONSE,
            $carrierDataObject->error()->getMessage()
        ));

        return $carrierDataObject->transaction();
    }

    private function endpoint(?string $endpoint): string
    {
        if (isset($endpoint)) {
            return Endpoints::BASE_SANDBOX.Endpoints::QUERY.$endpoint;
        } else {
            throw OnboardingSdkException::forDataNotProvided(ExceptionMessages::QUERY_ID);
        }
    }
}
