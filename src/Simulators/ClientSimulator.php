<?php

namespace Pabon\OnboardingSdk\Simulators;

use Pabon\OnboardingSdk\Constants\Endpoints;
use Pabon\OnboardingSdk\Simulators\Behaviours\AuthenticationBehaviour;
use Pabon\OnboardingSdk\Simulators\Behaviours\BaseSimulatorBehaviour;
use GuzzleHttp\Psr7\Response;
use PlacetoPay\Tangram\Mock\Client\HttpClientMock;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;

class ClientSimulator extends HttpClientMock
{
    protected const HANDLERS = [
        Endpoints::AUTHENTICATION => AuthenticationBehaviour::class,
    ];

    protected function createResponse(RequestInterface $request, array $options): ResponseInterface
    {
        /** @var BaseSimulatorBehaviour $behaviour */
        $behaviour = self::HANDLERS[$request->getUri()->getPath()] ?? null;

        if (! $behaviour) {
            return new Response(404);
        }

        return $behaviour::create()->resolve($request);
    }
}
