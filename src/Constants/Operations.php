<?php

namespace Pabon\OnboardingSdk\Constants;

class Operations
{
    public const VALIDATE_IDENTITY = 'validateIdentity';
    public const QUERY = 'query';
}
