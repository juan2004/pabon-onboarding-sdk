<?php

namespace Pabon\OnboardingSdk\Constants;

class ExceptionMessages
{
    public const QUERY_ID = 'The field queryID is mandatory and must be of type numeric';
}
