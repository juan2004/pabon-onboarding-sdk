<?php

namespace Pabon\OnboardingSdk\Constants;

class Fields
{
    public const PERSON = 'person';
    public const LOCALE = 'locale';
    public const APPROVAL_URL = 'approval_url';
    public const DENIAL_URL = 'denial_url';
}
