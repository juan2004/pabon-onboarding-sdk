<?php

namespace Pabon\OnboardingSdk\Constants;

class Endpoints
{
    public const AUTHENTICATION = '/auth';
    public const BASE_SANDBOX = '/api';
    public const BASE_MOCK_SERVER = '/mocks/evertec-onboarding/onboarding-api-docs/739855';

    public const VALIDATE_IDENTITY = '/request';
    public const QUERY = '/query/';
}
