<?php

namespace Pabon\OnboardingSdk\Constants;

class Urls
{
    public const SANDBOX = 'https://onboarding-uat.placetopay.ws';
    public const MOCK_SERVER = 'https://stoplight.io';
}
