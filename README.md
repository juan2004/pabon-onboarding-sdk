# Onboarding Sdk

Software development kit to connect with the Onboarding's API

## Installation
You should add PlacetoPay repository:
```json
{
    "repositories": [
        {
            "type": "composer",
            "url": "https://dev.placetopay.com/repository"
        }
    ]
}
```

Then, you can install the package via composer:
```
composer require placetopay/onboarding-sdk
```

## Usage
The first thing to do is create an instance of the Gateway class and pass the credentials (apiKey, url)
in the parameter as an array. Example:
```
return new Gateway([
    'apiKey' => 'MpZTLddJ8aOTIB4V',
    'url' => Urls::SANDBOX,
]);
```

## Available methods

### validateIdentity():
To validate identity, you must call the validateIdentity() method from the Gateway class instance and pass as a parameter an
instance of the OnboardingTransaction class that receives an array with the request fields. Example:

```
$data = new OnboardingTransaction([
    'person' => [
        'document_type' => 'CC',
        'document' => '1061111110',
        'document_issue_date' => '2007-04-25',
        'name' => 'Pedro Alberto',
        'surname' => 'Pérez Jiménez',
        'email' => 'juan.pabon@evertecinc.com',
        'mobile' => [
            'mobile' => '3152364205',
            'mobile_prefix' => 57,
            'mobile_country' => 'CO',
        ],
    ],
    'approval_url' => 'https://dev.placetopay',
    'denial_url' => 'https://dev.placetopay',
    'locale' => 'es',
]);

$response = $gateway->validateIdentity($data);
```

### query():
To query, you must call the query() method from the Gateway class instance and pass as a parameter an
instance of the OnboardingTransaction class that receives an array with the requestID to query. Example:

```
$response = $gateway->query(new OnboardingTransaction(['requestID' => 186]));
```

## Responses
The response obtained in each of the available methods always belongs to a 'Status' type entity that is understood by
the projects that incorporate the Placetopay 'Base' package. Which consists of: status, reason, message and date.
```
{
  +status: "OK"
  +reason: "00"
  +message: "{"data":{"request_id":267,
                    "url":"https:\/\/onboarding-uat.placetopay.ws\/validation\/255?signature=d3da8f17e0fa1e4bed97802e51a4968823df8eecbbd2255c9b93437aac80b713",
                    "initial_status":1}}"
  +date: "2022-11-24T22:41:12+00:00"
}
```

To get only the status
```
$response->status
```

To get only the reason
```
$response->reason
```

To get only the message
```
$response->message
```

To get only the date
```
$response->date
```

#### To obtain some specific data from the message, you must decode the JSON of the message property and access the property of the data that is required.
Example

```
json_decode($response->message)->data->url   //To get the url of an identity to validate
```

## Constants
The Urls class contains the following constants:
```
public const SANDBOX = 'https://onboarding-uat.placetopay.ws';
public const MOCK_SERVER = 'https://stoplight.io';
```
It is recommended to use them when giving a value to the "url" field in the Gateway instance.

## Important

* Remember that available methods must be passed an instance of OnboardingTransaction as a parameter to avoid an exception or error.


#### For more information about the Onboarding's API see the documentation [Here](https://evertec-onboarding.stoplight.io/docs/onboarding-api-docs/ZG9jOjE3Njc2MDc-identificaciones-de-prueba)