<?php

namespace Tests\Unit;

use Pabon\OnboardingSdk\Entities\OnboardingTransaction;
use Tests\TestCase;

class OnboardingTransactionTest extends TestCase
{
    private function dataRequest(): array
    {
        return [
            'person' => [
                'document_type' => 'CC',
                'document' => '1061111110',
                'document_issue_date' => '2007-04-25',
                'name' => 'Pedro Alberto',
                'surname' => 'Pérez Jiménez',
                'email' => 'user@test.com',
                'mobile' => [
                    'mobile' => '3152364205',
                    'mobile_prefix' => 57,
                    'mobile_country' => 'CO',
                ],
            ],
            'approval_url' => 'https://dev.placetopay',
            'denial_url' => 'https://dev.placetopay',
            'device_channel' => 'BROWSER',
            'locale' => 'es',
            'device_fingerprint' => [
                'abc' => 123,
            ],
            'requestID' => 186,
        ];
    }

    public function testItCanCorrectlyAnalyzeTheCreationOfAOnboardingTransaction(): void
    {
        $request = new OnboardingTransaction($this->dataRequest());

        $this->assertEquals($this->dataRequest()['person'], $request->getPerson());
        $this->assertEquals($this->dataRequest()['locale'], $request->getLocale());
        $this->assertEquals($this->dataRequest()['approval_url'], $request->getApprovalUrl());
        $this->assertEquals($this->dataRequest()['denial_url'], $request->getDenialUrl());
        $this->assertEquals($this->dataRequest()['requestID'], $request->getRequestID());
    }
}
