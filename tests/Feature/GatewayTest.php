<?php

namespace Tests\Feature;

use PlacetoPay\Tangram\Exceptions\InvalidSettingException;
use Pabon\OnboardingSdk\Gateway;
use Tests\TestCase;

class GatewayTest extends TestCase
{
    public function testTheApiKeyIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "apiKey" is missing.');

        $resolver = new Gateway([
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testItValidatesApiKeyIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "apiKey" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'apiKey' => ['placetopay'],
            'url' => 'https://dev.placetopay.com',
        ]);
    }

    public function testTheUrlIsMandatory(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The required option "url" is missing.');

        $resolver = new Gateway([
            'apiKey' => 'placetopay',
        ]);
    }

    public function testItValidatesUrlIsAString(): void
    {
        $this->expectException(InvalidSettingException::class);
        $this->expectExceptionMessage('The option "url" with value array is expected to be of type "string", but is of type "array".');

        $resolver = new Gateway([
            'apiKey' => 'placetopay',
            'url' => ['https://dev.placetopay.com'],
        ]);
    }
}
