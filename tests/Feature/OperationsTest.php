<?php

namespace Tests\Feature;

use PlacetoPay\Base\Constants\ReasonCodes;
use PlacetoPay\Base\Entities\Status;
use Pabon\OnboardingSdk\Constants\ExceptionMessages;
use Pabon\OnboardingSdk\Constants\Urls;
use Pabon\OnboardingSdk\Entities\OnboardingTransaction;
use Pabon\OnboardingSdk\Exceptions\OnboardingSdkException;
use Pabon\OnboardingSdk\Gateway;
use PlacetoPay\Tangram\Mock\TestLogger;
use Tests\TestCase;

class OperationsTest extends TestCase
{
    private function gateway(): Gateway
    {
        return new Gateway([
            'apiKey' => 'PCfbEn2fyyKVW25L',
            'url' => Urls::SANDBOX,
        ]);
    }

    private function gatewayWithLog(TestLogger $logger): Gateway
    {
        return new Gateway([
            'apiKey' => 'PCfbEn2fyyKVW25L',
            'url' => Urls::SANDBOX,
            'logger' => [
                'via' => $logger,
                'path' => 'fake/path/onboarding-sdk_log',
            ],
        ]);
    }

    private function dataRequest(): OnboardingTransaction
    {
        return new OnboardingTransaction([
            'person' => [
                'document_type' => 'CC',
                'document' => '1061111110',
                'document_issue_date' => '2007-04-25',
                'name' => 'Pedro Alberto',
                'surname' => 'Pérez Jiménez',
                'email' => 'user@test.com',
                'mobile' => [
                    'mobile' => '3152364205',
                    'mobile_prefix' => 57,
                    'mobile_country' => 'CO',
                ],
            ],
            'device_channel' => 'BROWSER',
            'locale' => 'es',
            'device_fingerprint' => [
                'abc' => 123,
            ],
        ]);
    }

    public function testItCanValidateIdentitySuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->validateIdentity($this->dataRequest());

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_OK, $response->status);
        $this->assertEquals(ReasonCodes::APPROVED_TRANSACTION, $response->reason);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response);
    }

    public function testItCannotValidateIdentityFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->validateIdentity(new OnboardingTransaction([]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_FAILED, $response->status);
        $this->assertEquals(ReasonCodes::INVALID_RESPONSE, $response->reason);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response);
    }

    public function testItCanDoLogOfValidateIdentity(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $response = $gateway->validateIdentity($this->dataRequest());

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }

    public function testItCanQuerySuccessful(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->query(new OnboardingTransaction(
            ['requestID' => json_decode($gateway->validateIdentity($this->dataRequest())->message)->data->request_id]
        ));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_OK, $response->status);
        $this->assertEquals(ReasonCodes::APPROVED_TRANSACTION, $response->reason);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response);
    }

    public function testItCannotQueryFailed(): void
    {
        $gateway = $this->gateway();

        $response = $gateway->query(new OnboardingTransaction(['requestID' => 0]));

        $this->assertInstanceOf(Status::class, $response);
        $this->assertEquals(Status::ST_FAILED, $response->status);
        $this->assertEquals(ReasonCodes::INVALID_RESPONSE, $response->reason);
        $this->assertObjectHasAttribute('message', $response);
        $this->assertObjectHasAttribute('date', $response);
    }

    public function testItCannotQueryWithoutRequestIdField(): void
    {
        $gateway = $this->gateway();

        $this->expectException(OnboardingSdkException::class);
        $this->expectExceptionMessage(ExceptionMessages::QUERY_ID);

        $response = $gateway->query(new OnboardingTransaction([]));
    }

    public function testItCanDoLogOfQuery(): void
    {
        $logger = new TestLogger;

        $gateway = $this->gatewayWithLog($logger);

        $response = $gateway->query(new OnboardingTransaction(
            ['requestID' => json_decode($gateway->validateIdentity($this->dataRequest())->message)->data->request_id]
        ));

        $this->assertInstanceOf(TestLogger::class, $logger);
        $this->assertObjectHasAttribute('records', $logger);
        $this->assertObjectHasAttribute('recordsByLevel', $logger);
    }
}
